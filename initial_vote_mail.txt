Greetings,

A vote entitled "__VOTENAME__" is now available for
your participation at
__VOTEURL__

This vote will expire at __VOTEEXPIRATION__.
You will receive one reminder email before then.

Thanks,
Ballot Voting System
